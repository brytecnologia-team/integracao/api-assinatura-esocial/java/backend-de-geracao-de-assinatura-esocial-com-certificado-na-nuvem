package br.com.bry.hub.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import br.com.bry.hub.configuration.ServiceConfiguration;

@Service
public class AssinadorKMSService {
	@Autowired
	private RestTemplate restTemplate;

	public String assinar(HttpServletRequest request) throws JSONException {

		ResponseEntity<String> responseInitialize = null;

		//Cria requisição para enviar ao servidor do HUB
		System.out.println("Parseando requisicao recebida do front");
		HttpEntity<?> requestToAPI = this.getHttpEntity(request);

		//Envia requisição ao HUB
		System.out.println("Enviando requisição ao HUB2");
		responseInitialize = restTemplate
				.exchange(ServiceConfiguration.URL_KMS, HttpMethod.POST, requestToAPI, String.class);

		//Obtem url de download do documento assinado para retornar ao front
		System.out.println("Obtendo link de download do documento assinado para retornar ao frontend");
		JSONObject body = new JSONObject(responseInitialize.getBody());
		return body.getJSONArray("documentos").getJSONObject(0).getJSONArray("links").getJSONObject(0).getString("href");

	}

	private HttpEntity<?> getHttpEntity(HttpServletRequest request) throws JSONException {

		//Obtem o documento XML
		List<MultipartFile> currentDocumentStreamContentValue = ((StandardMultipartHttpServletRequest) request).getMultiFileMap()
				.get("documento");
		Resource resourceOriginalDocument = null;
		if (currentDocumentStreamContentValue != null && !currentDocumentStreamContentValue.isEmpty()) {
			resourceOriginalDocument = currentDocumentStreamContentValue.get(0).getResource();
		}
		
		//Obtem o uuid do certificado
		String uuid = request.getParameterValues("uuid")[0];
		
		//Configura os parametros da requisição
		MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
		map.add("nonce", "1");
		map.add("uuid", uuid);
		map.add("signatureFormat", "ENVELOPED");
		map.add("hashAlgorithm", "SHA256");
		map.add("profile", "BASIC");
		map.add("returnType", "LINK");
		map.add("canonicalizerType", "INCLUSIVE");
		map.add("generateSimplifiedXMLDSig", "true");
		map.add("includeXPathEnveloped", "false");
		
		map.add("originalDocuments[0][nonce]", "1");
		map.add("originalDocuments[0][content]", resourceOriginalDocument);

		//Configura header da requisição
		final HttpHeaders headers = new HttpHeaders(); 	
		headers.set("Authorization", ServiceConfiguration.AUTHORIZATION);
		headers.set("kms_credencial_tipo", ServiceConfiguration.KMS_CREDENCIAL_TIPO);
		headers.add("kms_credencial", ServiceConfiguration.KMS_CREDENCIAL);	
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);

		System.out.println("Requisição para o HUB2 criada");
		return new HttpEntity<>(map, headers);
	}

}
