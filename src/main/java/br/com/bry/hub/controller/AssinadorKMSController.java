package br.com.bry.hub.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.bry.hub.service.AssinadorKMSService;

@RestController
@CrossOrigin(origins = "*")
public class AssinadorKMSController {

	@Autowired
	private AssinadorKMSService assinadorKMSService;

	//Recebe a requisição do front end
	@PostMapping(value = "/assinarKMS", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> initializeSignature(HttpServletRequest request, HttpServletResponse response) throws Exception {

		System.out.println("Requisição recebida no backend");
		return new ResponseEntity<String>(assinadorKMSService.assinar(request), HttpStatus.OK);
	}

}
