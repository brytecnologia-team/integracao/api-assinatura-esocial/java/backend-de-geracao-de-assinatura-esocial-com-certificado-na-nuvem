package br.com.bry.hub.configuration;

public class ServiceConfiguration {

	public static final String KMS_CREDENCIAL = "";
	public static final String AUTHORIZATION = ""; 
	public static final String KMS_CREDENCIAL_TIPO = "PIN";	//Valores disponíveis PIN ou TOKEN
	
	public static final String URL_KMS = "https://hub2.hom.bry.com.br/api/xml-signature-service/v1/signatures/kms"; 
	//public static final String URL_KMS = "https://hub2.bry.com.br/api/xml-signature-service/v1/signatures/kms";
}
