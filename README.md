# API de Geração de Assinatura eSocial com certificado em nuvem

Este é um exemplo backend de integração dos serviços da API de assinatura com clientes baseados em tecnologia Java, que utilizam o KMS. Nele são apresentados os passos necessários para a geração de uma assinatura eSocial utilizando-se do certificado em nuvem no KMS.

### Tech

O exemplo utiliza das bibliotecas Java abaixo:
* [SpringBoot Web] - Spring RestTemplate for create requests
* [JDK 8] - Java 8


**Observação**

Esta API em Java pode ser executada juntamente com o [frontend](https://gitlab.com/brytecnologia-team/integracao/api-assinatura-esocial/react/geracao-de-assinatura-esocial-com-certificado-na-nuvem) de exemplo feito em React

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

Além disso, no processo de geração de assinatura é obrigatório a posse de um certificado digital que identifica o autor do artefato assinado que será produzido.

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a produção da assinatura.

| Variável | Descrição | Classe de Configuração |
| ------ | ------ | ------ |
| Authorization | Token de acesso (access_token), obtido no BRyCloud (https://api-assinatura.bry.com.br/api-assinatura-digital#autenticacao-apis). Para uso corporativo, o token deve ser da pessoa  jurídica e para uso pessoal, da pessoa física. | ServiceConfiguration.java
| kmsCredencial | PIN cadastrado no KMS, codificado em Base64 | ServiceConfiguration.java
| kmsCredencialTipo | Tipo de crendecial utilizado no KMS. [Disponíveis: PIN e TOKEN] | ServiceConfiguration.java

## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  


### Uso

Para execução da aplicação de exemplo, compile diretamente as classes ou importe o projeto em sua IDE de preferência.
Utilizamos o JRE versão 8 para desenvolvimento e execução.


[SpringBoot Web]: <https://spring.io/>
[JDK 8]: <https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html>
